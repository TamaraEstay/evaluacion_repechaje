package com.example.apptamaraestayrepechaje;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import com.example.apptamaraestayrepechaje.controller.AuthController;
import com.example.apptamaraestayrepechaje.models.responses.AuthResponse;
import com.google.android.material.textfield.TextInputLayout;

public class LoginActivity extends AppCompatActivity {

    private Button btnEnterLogin, btnEnterRegister;
    private TextInputLayout txtUserLogin, txtPasswordLogin;
    private AuthController authController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);



        //asociar componentes entre view y activity
        txtUserLogin = findViewById(R.id.activity_login_til_user);
        txtPasswordLogin = findViewById(R.id.activity_login_til_password);
        btnEnterLogin = findViewById(R.id.activity_login_btn_login);
        btnEnterRegister = findViewById(R.id.activity_login_btn_register);

        //agregando el evento del boton iniciar sesion
        btnEnterLogin.setOnClickListener(view -> {
            Toast.makeText(view.getContext(), R.string.activity_login_txt_toast_confirm_login, Toast.LENGTH_SHORT).show();
            String username = txtUserLogin.getEditText().getText().toString();
            String password = txtPasswordLogin.getEditText().getText().toString();
            //creo la instancia del controler
            authController = new AuthController(view.getContext());
            //aqui hago el login
            AuthResponse authResponse = authController.initLogin(username, password);
            if(authResponse.isStatus()){
                Intent intentLogin = new Intent(view.getContext(), MainActivity.class);
                startActivity(intentLogin);
                finish();
            }
            Toast.makeText(view.getContext(), authResponse.getMessage(), Toast.LENGTH_SHORT).show();
        });

        //agregando el evento del boton registrarse
        btnEnterRegister.setOnClickListener(view -> {
            Intent intent = new Intent(view.getContext(), RegisterActivity.class);
            startActivity(intent);
            finish();
        });
    }
}