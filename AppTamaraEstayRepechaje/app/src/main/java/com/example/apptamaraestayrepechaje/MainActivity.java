package com.example.apptamaraestayrepechaje;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ListView;

import com.example.apptamaraestayrepechaje.controller.AuthController;
import com.example.apptamaraestayrepechaje.controller.BmiController;
import com.example.apptamaraestayrepechaje.models.Bmi;
import com.example.apptamaraestayrepechaje.models.BmiEntity;
import com.example.apptamaraestayrepechaje.utils.BmiAdapter;
import com.example.apptamaraestayrepechaje.utils.DatePickerFragment;
import com.example.apptamaraestayrepechaje.utils.DateUtils;
import com.google.android.material.textfield.TextInputLayout;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private Button btnLogout, btnFilterEvaluation, btnCreateEvaluation;
    private TextInputLayout tilStartDate, tilEndDate;
    private ListView lvAllBmi;
    private BmiController bmiController;
    private AuthController authController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //asociamos los componentes con atributos
        lvAllBmi = findViewById(R.id.activity_main_lv_bmi);
        tilStartDate = findViewById(R.id.activity_main_til_start_date);
        tilEndDate = findViewById(R.id.activity_main_til_end_date);
        btnFilterEvaluation = findViewById(R.id.activity_main_btn_filter);
        btnCreateEvaluation = findViewById(R.id.activity_main_btn_new_bmi);
        btnLogout = findViewById(R.id.activity_main_btn_sign_out);

        bmiController = new BmiController(this);
        List<Bmi> bmiList = bmiController.getAll();
        this.setAllBmiAdapter(bmiList);

        BmiAdapter adapter = new BmiAdapter(this, bmiList);

        lvAllBmi.setAdapter(adapter);

        lvAllBmi.setOnItemClickListener(((adapterView, view, index, id) -> {
            Bmi bmi = bmiList.get(index);
            Intent i = new Intent(view.getContext(), ActivityBmiDetail.class);
            i.putExtra("bmi", (Serializable) bmi);
            view.getContext().startActivity(i);
        }));



        ///// cargo los datepicker
        tilStartDate.getEditText().setOnClickListener(view -> {
            DatePickerFragment.showDatePickerDialog(this, tilStartDate, new Date());
        });
        ///// cargo los datepicker
        tilEndDate.getEditText().setOnClickListener(view -> {
            DatePickerFragment.showDatePickerDialog(this, tilEndDate, new Date());
        });

        btnCreateEvaluation.setOnClickListener(view->{
            Intent intent = new Intent(view.getContext(), ActivityBmi.class);
            startActivity(intent);
            finish();
        });

        btnLogout.setOnClickListener(view->{
            authController = new AuthController(view.getContext());
            Intent i = new Intent(view.getContext(), LoginActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            view.getContext().startActivity(i);
            ((Activity) view.getContext()).finish();
        });

        btnFilterEvaluation.setOnClickListener(view->{
            Date from = DateUtils.unsafeParse(tilStartDate.getEditText().getText().toString());
            Date to = DateUtils.unsafeParse(tilEndDate.getEditText().getText().toString());
            List<Bmi> bmiRangeList = bmiController.getRange(from, to);
            this.setAllBmiAdapter(bmiRangeList);
        });

    }


    @Override
    protected void onResume() {
        super.onResume();
        List<Bmi> bmiList = bmiController.getAll();
        this.setAllBmiAdapter(bmiList);
    }


    private void setAllBmiAdapter(List<Bmi> bmiList) {
        BmiAdapter adapter = new BmiAdapter(this, bmiList);
        lvAllBmi.setAdapter(adapter);

        lvAllBmi.setOnItemClickListener(((adapterView, view, index, id) -> {
            Bmi bmi = bmiList.get(index);

            Intent i = new Intent(view.getContext(), ActivityBmiDetail.class);
            i.putExtra("bmi", (Serializable) bmi);
            view.getContext().startActivity(i);
        }));
    }
}