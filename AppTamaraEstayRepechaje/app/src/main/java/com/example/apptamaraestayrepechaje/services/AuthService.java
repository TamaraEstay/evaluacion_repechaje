package com.example.apptamaraestayrepechaje.services;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.apptamaraestayrepechaje.config.AppTamaraEstayRepechajeDatabase;
import com.example.apptamaraestayrepechaje.dao.UserDao;
import com.example.apptamaraestayrepechaje.models.responses.AuthResponse;
import com.example.apptamaraestayrepechaje.models.UserEntity;
import com.example.apptamaraestayrepechaje.utils.BCrypt;

public class AuthService {

    private final String KEY_USER_ID = "userId";
    private final String KEY_USERNAME = "userName";
    private final String KEY_FIRST_NAME = "userFirstName";
    private final String KEY_LAST_NAME = "userLastName";
    private final String KEY_EMAIL = "userEmail";
    private final String LOGIN_OK = "Login exitoso";
    private final String LOGIN_NO_OK = "Usuario o password invalida";

    private UserDao userDao;
    private Context ctx;
    private SharedPreferences preferences;

    public AuthService(Context ctx){
        this.ctx = ctx;
        int PRIVATE_MODE = 0;
        String PREF_NAME = "AppTamaraPref";
        this.preferences = ctx.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        this.userDao = AppTamaraEstayRepechajeDatabase.getInstance(ctx).userDao();
    }

    /**
     * Metodo que inicializa la sesion de usuario
     * @param userEntity
     */
    private void setUserSession(UserEntity userEntity) {
        SharedPreferences.Editor editor = this.preferences.edit();
        editor.putString(KEY_USERNAME, userEntity.getUsername());
        editor.putLong(KEY_USER_ID,userEntity.getId());
        editor.putString(KEY_EMAIL,userEntity.getEmail());
        editor.putString(KEY_FIRST_NAME,userEntity.getFirstName());
        editor.putString(KEY_LAST_NAME,userEntity.getLastName());
        editor.apply();
    }

    public UserEntity getUserSession() {
        UserEntity userEntitySesion = new UserEntity();
        userEntitySesion.setId(preferences.getLong(KEY_USER_ID,0));
        userEntitySesion.setUsername(preferences.getString(KEY_USERNAME, "")); // to fix
        userEntitySesion.setFirstName(preferences.getString(KEY_FIRST_NAME,""));
        userEntitySesion.setLastName(preferences.getString(KEY_LAST_NAME,""));
        userEntitySesion.setEmail(preferences.getString(KEY_EMAIL,""));
        return userEntitySesion;
    }

    public AuthResponse login(String userName, String password){
        AuthResponse authResponse = new AuthResponse();
        authResponse.setMessage(LOGIN_NO_OK);
        authResponse.setStatus(false);
        UserEntity userEntity = userDao.findByUsername(userName);
        if(userEntity != null){
            if(BCrypt.checkpw(password, userEntity.getPassword())){
                //generando la sesion
                this.setUserSession(userEntity);
                authResponse.setMessage(LOGIN_OK);
                authResponse.setStatus(true);
            }
        }
        return authResponse;
    }

    public AuthResponse register(UserEntity userEntity){
        final String REGISTER_OK = "Registro exitoso";
        AuthResponse authResponseRegister = new AuthResponse();
        //creo el hash para la contraseña ingresada por el usuario
        String hashedPassword = BCrypt.hashpw(userEntity.getPassword(), BCrypt.gensalt());
        //se la cargo al objeto que se va a registrar
        userEntity.setPassword(hashedPassword);
        //registro usuario
        userDao.insert(userEntity);
        authResponseRegister.setStatus(true);
        authResponseRegister.setMessage(REGISTER_OK);
        return authResponseRegister;
    }

    public void logout(){
        SharedPreferences.Editor editor = this.preferences.edit();
        editor.clear();
        editor.apply();
    }
}
