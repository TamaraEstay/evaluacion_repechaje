package com.example.apptamaraestayrepechaje.validations;

import com.example.apptamaraestayrepechaje.models.BmiEntity;

public class BmiValidations {


    public boolean validBmiEntity(BmiEntity bmiEntity){
        boolean result = false;
        if(bmiEntity.getWeight() != 0){
            if(bmiEntity.getDate() != null){
                result = true;
            }
        }
        return result;
    }
}
