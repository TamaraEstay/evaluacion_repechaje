package com.example.apptamaraestayrepechaje.controller;

import android.content.Context;

import com.example.apptamaraestayrepechaje.models.responses.AuthResponse;
import com.example.apptamaraestayrepechaje.models.UserEntity;
import com.example.apptamaraestayrepechaje.services.AuthService;
import com.example.apptamaraestayrepechaje.validations.AuthValidations;

public class AuthController {

    private AuthValidations authValidations;
    private AuthService authService;

    public AuthController(Context ctx){
        authValidations = new AuthValidations();
        authService = new AuthService(ctx);
    }

    public AuthResponse initLogin(String txtUserLogin, String txtPasswordLogin){
        final String LOGIN_DATA = "Favor agrege un usuario y password";
        AuthResponse authResponse = new AuthResponse();
        if(this.authValidations.validateLogin(txtUserLogin, txtPasswordLogin)){
            //llamamos al nuestra capa de servicio
            authResponse = authService.login(txtUserLogin, txtPasswordLogin);
        }else{
            authResponse.setStatus(false);
            authResponse.setMessage(LOGIN_DATA);
        }
        return authResponse;
    }

    public AuthResponse register(UserEntity userEntity){
        final String VALID_DATA_NO_OK = "favor revisar sus datos ingresados";
        AuthResponse authResponseRegister = null;
        if(this.authValidations.validateRegister(userEntity)){
            //llamamos a nuestra clase servicio y registramos el usuario
            authResponseRegister = authService.register(userEntity);
        }else{
            authResponseRegister.setStatus(false);
            authResponseRegister.setMessage(VALID_DATA_NO_OK);
        }
        return authResponseRegister;
    }

    public void logout(){
        this.authService.logout();
    }
}
