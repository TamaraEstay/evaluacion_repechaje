package com.example.apptamaraestayrepechaje;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import com.example.apptamaraestayrepechaje.controller.BmiController;
import com.example.apptamaraestayrepechaje.models.BmiEntity;
import com.example.apptamaraestayrepechaje.models.responses.BmiResponse;
import com.example.apptamaraestayrepechaje.utils.DatePickerFragment;
import com.example.apptamaraestayrepechaje.utils.DateUtils;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Date;

public class ActivityBmi extends AppCompatActivity {

    private Button btnSaveBmi, btnBack;
    private TextInputLayout tilDate, tilWeight;
    private BmiController bmiController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bmi);

        btnSaveBmi = findViewById(R.id.activity_bmi_create_btn_save);
        btnBack = findViewById(R.id.activity_bmi_create_btn_back);
        tilDate = findViewById(R.id.activity_bmi_create_til_date);
        tilWeight = findViewById(R.id.activity_bmi_create_til_weight);


        tilDate.getEditText().setOnClickListener(view -> {
            DatePickerFragment.showDatePickerDialog(this, tilDate, new Date());
        });

        btnSaveBmi.setOnClickListener(view ->{
            BmiEntity bmiEntity = new BmiEntity();
            bmiEntity.setWeight(Double.parseDouble(tilWeight.getEditText().getText().toString()));
            bmiEntity.setDate(DateUtils.unsafeParse(tilDate.getEditText().getText().toString()));
            bmiController = new BmiController(view.getContext());
            BmiResponse bmiResponseRegisterEvaluation = bmiController.registerEvaluation(bmiEntity);
            if(bmiResponseRegisterEvaluation.isStatus()){
                Intent intentRegisterEvaluation = new Intent(view.getContext(), MainActivity.class);
                startActivity(intentRegisterEvaluation);
                finish();
            }
            Toast.makeText(view.getContext(), bmiResponseRegisterEvaluation.getMessage(), Toast.LENGTH_SHORT).show();
        });

        btnBack.setOnClickListener(view -> {
            super.onBackPressed();
        });
    }
}