package com.example.apptamaraestayrepechaje.controller;

import android.content.Context;

import com.example.apptamaraestayrepechaje.models.Bmi;
import com.example.apptamaraestayrepechaje.models.BmiEntity;
import com.example.apptamaraestayrepechaje.models.responses.BmiResponse;
import com.example.apptamaraestayrepechaje.services.BmiService;
import com.example.apptamaraestayrepechaje.validations.BmiValidations;

import java.util.Date;
import java.util.List;

public class BmiController {
    private BmiService bmiService;
    private BmiValidations bmiValidations;

    public BmiController(Context ctx){
        this.bmiService = new BmiService(ctx);
        this.bmiValidations = new BmiValidations();
    }

    public BmiResponse registerEvaluation(BmiEntity bmiEntity){
        BmiResponse bmiResponseRegisterEvaluation = new BmiResponse();
        if(this.bmiValidations.validBmiEntity(bmiEntity)){
            bmiResponseRegisterEvaluation = this.bmiService.registerEvaluation(bmiEntity);
        }else{
            final String NO_VALIDATION_REGISTER_BMI = "Favor adjuntar datos para agregar una evaluacion";
            bmiResponseRegisterEvaluation.setStatus(false);
            bmiResponseRegisterEvaluation.setMessage(NO_VALIDATION_REGISTER_BMI);
        }
        return bmiResponseRegisterEvaluation;
    }


    public List<Bmi> getAll() {
        List<Bmi> listBmi;
        listBmi = this.bmiService.getAll();
        return listBmi;
    }

    public void deleteEvaluation(long idEvaluation){
        this.bmiService.deleteEvaluation(idEvaluation);
    }

    public List<Bmi> getRange(Date from, Date to){
        List<Bmi> listBmi;
        listBmi = this.bmiService.getFindByRange(from, to);
        return listBmi;
    }
}
