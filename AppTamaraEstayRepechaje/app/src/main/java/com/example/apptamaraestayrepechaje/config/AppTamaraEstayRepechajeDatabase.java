package com.example.apptamaraestayrepechaje.config;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.example.apptamaraestayrepechaje.dao.BmiDao;
import com.example.apptamaraestayrepechaje.dao.UserDao;
import com.example.apptamaraestayrepechaje.models.BmiEntity;
import com.example.apptamaraestayrepechaje.models.UserEntity;
import com.example.apptamaraestayrepechaje.utils.Converters;

@Database(entities = {UserEntity.class, BmiEntity.class}, version = 1, exportSchema = false)
@TypeConverters({Converters.class})
public abstract class AppTamaraEstayRepechajeDatabase extends RoomDatabase {
    private static final String DB_NAME = "AppTamaraEstay";
    private static AppTamaraEstayRepechajeDatabase instance;

    public static synchronized AppTamaraEstayRepechajeDatabase getInstance(Context ctx) {
        if (instance == null) {
            instance = Room.databaseBuilder(ctx.getApplicationContext(), AppTamaraEstayRepechajeDatabase.class, DB_NAME)
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }

    public abstract UserDao userDao();
    public abstract BmiDao bmiDao();
}
