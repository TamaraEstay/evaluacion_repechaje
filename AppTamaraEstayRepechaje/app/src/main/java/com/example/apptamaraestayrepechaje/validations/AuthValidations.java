package com.example.apptamaraestayrepechaje.validations;

import com.example.apptamaraestayrepechaje.models.UserEntity;

public class AuthValidations {

    private final String NOT_NULL = "^\\S*$";

    public boolean validateLogin(String userName, String password){
        boolean result = false;
        if(!userName.isEmpty() && userName.matches(NOT_NULL)){
            if(!password.isEmpty()){
                result = true;
            }
        }
        return result;
    }

    public boolean validateRegister(UserEntity userEntity){
        boolean resultRegister = true;
        return resultRegister;
    }
}
