package com.example.apptamaraestayrepechaje.services;

import android.content.Context;

import com.example.apptamaraestayrepechaje.config.AppTamaraEstayRepechajeDatabase;
import com.example.apptamaraestayrepechaje.dao.BmiDao;
import com.example.apptamaraestayrepechaje.dao.UserDao;
import com.example.apptamaraestayrepechaje.models.Bmi;
import com.example.apptamaraestayrepechaje.models.BmiEntity;
import com.example.apptamaraestayrepechaje.models.responses.BmiResponse;
import com.example.apptamaraestayrepechaje.models.UserEntity;
import com.example.apptamaraestayrepechaje.utils.CalculateBmi;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BmiService {

    private Context ctx;
    private BmiDao bmiDao;
    private UserDao userDao;
    private AuthService authService;

    public BmiService(Context ctx){
        this.ctx = ctx;
        this.bmiDao = AppTamaraEstayRepechajeDatabase.getInstance(ctx).bmiDao();
        this.userDao = AppTamaraEstayRepechajeDatabase.getInstance(ctx).userDao();
        this.authService = new AuthService(ctx);
    }

    public BmiResponse registerEvaluation(BmiEntity bmiEntity){
        BmiResponse bmiResponse = new BmiResponse();
        UserEntity userEntitySesion = authService.getUserSession();
        bmiEntity.setUserId(userEntitySesion.getId());
        this.bmiDao.insert(bmiEntity);
        bmiResponse.setStatus(true);
        bmiResponse.setMessage("Guardado con exito");
        return bmiResponse;
    }

    public BmiResponse deleteEvaluation(long idBmi){
        BmiResponse bmiResponse = new BmiResponse();
        this.bmiDao.delete(idBmi);
        bmiResponse.setStatus(true);
        bmiResponse.setMessage("Eliminado con exito");
        return bmiResponse;
    }


    public List<Bmi> getFindByRange(Date from, Date to){
        UserEntity userEntitySesion = authService.getUserSession();
        UserEntity userEntityBd = this.userDao.findByUsername(userEntitySesion.getUsername());
        List<BmiEntity> listEvaluation = this.bmiDao.findByRange(from, to, userEntitySesion.getId());
        List<Bmi> listBmiGenerate = this.generateBmi(listEvaluation, userEntityBd.getHeight());
        return listBmiGenerate;
    }

    public List<Bmi> getAll(){
        List<Bmi> listBmi = new ArrayList<>();
        UserEntity userEntitySesion = authService.getUserSession();
        UserEntity userEntityBd = this.userDao.findByUsername(userEntitySesion.getUsername());
        List<BmiEntity> listBmiEntity = this.bmiDao.findAll(userEntitySesion.getId());
        List<Bmi> listBmiGenerate = this.generateBmi(listBmiEntity, userEntityBd.getHeight());
        return listBmiGenerate;
    }


    private List<Bmi> generateBmi(List<BmiEntity> listEntityBmi, double heigth){
        List<Bmi> listBmi = new ArrayList<>();
        if(!listEntityBmi.isEmpty()){
            for(BmiEntity bmiE : listEntityBmi){
                Bmi bmi = new Bmi();
                bmi.setId(bmiE.getId());
                bmi.setDate(bmiE.getDate());
                bmi.setWeight(bmiE.getWeight());
                bmi.setUserId(bmiE.getUserId());
                bmi.setCalculatedBmi(CalculateBmi.calculateBmi(bmiE.getWeight(), heigth));
                listBmi.add(bmi);
            }
        }
        return listBmi;
    }
}
