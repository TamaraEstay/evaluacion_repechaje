package com.example.apptamaraestayrepechaje;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import com.example.apptamaraestayrepechaje.controller.AuthController;
import com.example.apptamaraestayrepechaje.models.responses.AuthResponse;
import com.example.apptamaraestayrepechaje.models.UserEntity;
import com.example.apptamaraestayrepechaje.utils.DatePickerFragment;
import com.google.android.material.textfield.TextInputLayout;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RegisterActivity extends AppCompatActivity {

    private final String DATE_PATTERN = "yyyy-MM-dd";
    private TextInputLayout tilUsername, tilFirstName, tilLastName, tilDateOfBirth, tilHeight, tilEmail, tilPassword;
    private Button btnSendSignUpForm;
    private AuthController authController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //enlazo los componentes
        btnSendSignUpForm = findViewById(R.id.activity_sign_up_btn_confirm_sign_up);
        tilUsername = findViewById(R.id.activity_sign_up_til_username);
        tilFirstName = findViewById(R.id.activity_sign_up_til_first_name);
        tilLastName = findViewById(R.id.activity_sign_up_til_last_name);
        tilDateOfBirth = findViewById(R.id.activity_sign_up_til_date_of_birth);
        tilHeight = findViewById(R.id.activity_sign_up_til_height);
        tilEmail = findViewById(R.id.activity_sign_up_til_email);
        tilPassword = findViewById(R.id.activity_sign_up_til_password);

        //evento de datePicker
        tilDateOfBirth.getEditText().setOnClickListener(view -> {
            DatePickerFragment.showDatePickerDialog(this, tilDateOfBirth, new Date());
        });

        //evento de boton para registrar
        btnSendSignUpForm.setOnClickListener(view->{
            UserEntity userEntity = new UserEntity();
            userEntity.setUsername(tilUsername.getEditText().getText().toString());
            userEntity.setFirstName(tilFirstName.getEditText().getText().toString());
            userEntity.setLastName(tilLastName.getEditText().getText().toString());
            Date dateOfBirthFormatted = null;
            try {
                dateOfBirthFormatted = new SimpleDateFormat(DATE_PATTERN).parse(tilDateOfBirth.getEditText().getText().toString());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            userEntity.setDateOfBirth(dateOfBirthFormatted);
            userEntity.setHeight(Double.parseDouble(tilHeight.getEditText().getText().toString()));
            userEntity.setEmail(tilEmail.getEditText().getText().toString());
            userEntity.setPassword(tilPassword.getEditText().getText().toString());

            //inicializo el componente controller
            authController = new AuthController(view.getContext());
            AuthResponse authResponse = authController.register(userEntity);
            if(authResponse.isStatus()){
                Intent intentLogin = new Intent(view.getContext(), LoginActivity.class);
                startActivity(intentLogin);
                finish();
            }
            Toast.makeText(view.getContext(), authResponse.getMessage(), Toast.LENGTH_SHORT).show();
        });
    }
}